# CHANGELOG



## v0.2.2 (2024-04-01)

### Fix

* fix(deploy): add deploy pipeline ([`5248ad1`](https://gitlab.com/jculici/my-package/-/commit/5248ad1bc87804c63ad9299876d136375b707c54))

### Unknown

* Add new directory ([`bbf39f3`](https://gitlab.com/jculici/my-package/-/commit/bbf39f363dd4da385270824d8786a6ca98c6cd7a))

* muevo archivos fuera de la carpeta ([`888f8e6`](https://gitlab.com/jculici/my-package/-/commit/888f8e6201759a33a33007bca2349176db49cab4))

* Agrego carpeta my-package y muevo archivos ([`e2d7cc8`](https://gitlab.com/jculici/my-package/-/commit/e2d7cc81554236901ba96392c74174c4eb377c86))


## v0.2.1 (2024-03-24)

### Fix

* fix(deploy): add deploy pipeline ([`3e5d659`](https://gitlab.com/jculici/my-package/-/commit/3e5d65939d3302450ce2956d9913430774f0d287))

* fix(deploy): add deploy pipeline ([`b4b30ad`](https://gitlab.com/jculici/my-package/-/commit/b4b30ad17a8841854f8b88897338c40243e7e1c9))


## v0.2.0 (2024-03-24)

### Feature

* feat(test): add new test module ([`5f6531a`](https://gitlab.com/jculici/my-package/-/commit/5f6531a3314158c55406bc698e5ddcea1e56b1c4))

### Unknown

* Merge branch &#39;main&#39; of gitlab.com:jculici/my-package ([`3609fc0`](https://gitlab.com/jculici/my-package/-/commit/3609fc0d087d521185bbb1963365b034eec7f1fb))


## v0.1.0 (2024-03-24)

### Feature

* feat(sr): add semantic release cd pipeline ([`969b4f5`](https://gitlab.com/jculici/my-package/-/commit/969b4f514e1659203e814425fd970d6647cc8d4d))

### Unknown

* Add semantic release config ([`f239638`](https://gitlab.com/jculici/my-package/-/commit/f23963840167a5a8cc794e7990389ce8f89d0804))

* Add poetry semantic release ([`03103ff`](https://gitlab.com/jculici/my-package/-/commit/03103ff43e5f33c6d435e0953abb7b55cfb20b2e))

* Add pyproject.toml to init poetry ([`e841807`](https://gitlab.com/jculici/my-package/-/commit/e841807e392cc67487c5e8a02a37f91513ef9d8a))

* Update .gitlab-ci.yml file ([`f198f88`](https://gitlab.com/jculici/my-package/-/commit/f198f88d7755ee36018675fe0a1adf9ee4422fc6))

* Add first pipeline ([`27d6c7c`](https://gitlab.com/jculici/my-package/-/commit/27d6c7c08803b8afd11d3d178ec68d8e32dea0a8))

* Initial commit ([`d57ec44`](https://gitlab.com/jculici/my-package/-/commit/d57ec442da43d96f83d7ee350dba24329d8716cc))
